# jsv-mod_oauth2

This package contains [mod_oauth2](https://github.com/zmartzone/mod_oauth2) and aims to closely resemble the official RPM published for each release by the upstream.
It is built against jansson with versioned symbols, although it can use a jansson library without versioned symbols as well.

As it builds on `jsv-liboauth2`, `jsv-mod_oauth2` requires the EPEL repository.
