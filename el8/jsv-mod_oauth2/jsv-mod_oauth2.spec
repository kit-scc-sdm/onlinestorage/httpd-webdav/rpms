Name:           jsv-mod_oauth2
Version:        3.3.1
Release:        1%{?dist}
Summary:        OAuth 2.0 Resource Server module for the Apache HTTPd web server
Group:          Networking/Daemons/HTTP
License:        AGPL-3.0-or-later
URL:            https://github.com/zmartzone/mod_oauth2
Source0:        https://github.com/zmartzone/mod_oauth2/releases/download/v%{version}/mod_oauth2-%{version}.tar.gz
Source1:        10-oauth2.conf


BuildRequires:  gcc
BuildRequires:  autoconf automake libtool
BuildRequires:  make
BuildRequires:  httpd-devel
BuildRequires:  jsv-liboauth2-devel

# Exclude libjansson with version information from the automatically
# generated Requires list. libjansson without version will still be required.
%global __requires_exclude ^libjansson\\.so\\..+\\(.+\\)\\(.*\\)$

# mod_oauth2 and jsv-mod_oauth2 provide the same files.
Conflicts:      mod_oauth2

%description
A module for Apache HTTP Server 2.x that makes the Apache web server operate
as a OAuth 2.0 Resource Server, validating OAuth 2.0 bearer access tokens and
setting headers/environment variables based on the validation results.


%prep
%autosetup -p1 -n mod_oauth2-%{version}

%build
./autogen.sh
%configure
%make_build


%install
%make_install
install -m 644 -D %{SOURCE1} %{buildroot}/%{_httpd_modconfdir}/10-oauth2.conf

%files
%license LICENSE
%doc ChangeLog README.md
%doc oauth2.conf
%{_httpd_moddir}/mod_oauth2.so
%config(noreplace) %{_httpd_modconfdir}/10-oauth2.conf

%changelog
* Mon Feb 06 2023 Paul Skopnik <paul.skopnik@kit.edu> - 3.3.1-1
- Create initial spec resembling upstream-released RPMs
- Rename to jsv-cjose as part of the JSON Symbol Versioning package set

