# jsv-gfal2

This package contains the [GFAL2](https://gitlab.cern.ch/dmc/gfal2) library built against `jsv-json-c`.

The `jsv-json-c` package is a required dependency because the `json-c` package in EL7 has a different SONAME.
However, most programs which build against GFAL2 will not directly access the `json-c` library and can be used with either `gfal2` or `jsv-gfal2` regardless of which GFAL2 package was used during building.

Based on the gfal2-2.21.2 spec in EPEL 7.

 * https://src.fedoraproject.org/rpms/gfal2
   * https://src.fedoraproject.org/rpms/gfal2/tree/epel7
