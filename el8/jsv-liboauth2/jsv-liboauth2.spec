Name:           jsv-liboauth2
Version:        1.4.5.4
Release:        1%{?dist}
Summary:        C library for OAuth 2.x
Group:          System Environment/Libraries
License:        AGPL-3.0-or-later
URL:            https://github.com/zmartzone/liboauth2
Source0:        https://github.com/zmartzone/liboauth2/releases/download/v%{version}/liboauth2-%{version}.tar.gz


BuildRequires:  gcc
BuildRequires:  autoconf automake libtool
BuildRequires:  check-devel
BuildRequires:  make
BuildRequires:  jsv-cjose-devel
BuildRequires:  hiredis-devel
BuildRequires:  httpd-devel
BuildRequires:  jsv-jansson-devel
BuildRequires:  libcurl-devel
BuildRequires:  libmemcached-devel
BuildRequires:  openssl-devel

# Exclude libjansson with version information from the automatically
# generated Requires list. libjansson without version will still be required.
%global __requires_exclude ^libjansson\\.so\\..+\\(.+\\)\\(.*\\)$

# liboauth2 and jsv-liboauth2 provide the same files.
Conflicts:      liboauth2

%description
Generic library to build C-based OAuth 2.x and OpenID Connect servers and clients e.g. web-server plugins.


%package        apache
Summary:        Apache bindings for %{name}
Group:          System Environment/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    apache
Apache bindings for %{name}


%package        devel
Summary:        Development files for %{name}
Group:          System Environment/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -p1 -n liboauth2-%{version}

%build
./autogen.sh
%configure
%make_build


%install
%make_install
find %{buildroot} -name '*.a' -exec rm -f {} ';'
find %{buildroot} -name '*.la' -exec rm -f {} ';'


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%check
# Tests require redis and memcached running, can those tests be disabled?
#make check || (cat test-suite.log; exit 1)

%files
%license LICENSE
%doc ChangeLog README.md
%{_libdir}/liboauth2.so.*

%files apache
%{_libdir}/liboauth2_apache.so.*

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Sat Feb 04 2023 Paul Skopnik <paul.skopnik@kit.edu> - 1.4.5.4-1
- Create initial spec resembling upstream-released RPMs
- Rename to jsv-cjose as part of the JSON Symbol Versioning package set

