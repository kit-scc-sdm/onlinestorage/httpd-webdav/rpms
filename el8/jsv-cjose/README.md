# jsv-cjose

This package contains [zmartzone cjose](https://github.com/zmartzone/cjose), a fork of the [original cisco cjose project](https://github.com/cisco/cjose).
It is built against jansson with versioned symbols, although it can use a jansson library without versioned symbols as well.

Based on the cjose-0.6.1 spec (for the original cjose) in Fedora.

 * https://src.fedoraproject.org/rpms/cjose
