# jsv-jansson

This package of [jansson](https://github.com/akheron/jansson) exports versioned symbols to avoid conflicts with other JSON libraries.

The `jansson` package from base is often already installed because it is required by other installed packages.
Replacing the package with `jsv-jansson` must happen in a single transaction so that there are no unfulfilled dependencies.
This can be achieved with `yum swap`:

```shell
$ yum swap -- remove jansson -- install jsv-jansson
```

Originally based on the jansson-2.10-1 spec in CentOS 7 with updates from jansson-2.14-1 spec in CentOS 8.

 * https://git.centos.org/rpms/jansson
   * https://git.centos.org/rpms/jansson/tree/c7
