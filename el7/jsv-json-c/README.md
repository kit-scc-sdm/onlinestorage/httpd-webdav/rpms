# jsv-json-c

This package contains [json-c](https://github.com/json-c/json-c) with versioned symbols.

Because EL7 ships a `json-c` package with a different soname, i.e. incompatibilities in the ABI, a drop-in replacement is not possible.
Instead, `jsv-json-c` installs the library under the filename and soname `libjsv-json-c.so.5`, so that both packages can be installed at the same time.

It is based on the json-c-0.16 spec in Fedora 37.

 * https://src.fedoraproject.org/rpms/json-c
   * https://src.fedoraproject.org/rpms/json-c/tree/f37
