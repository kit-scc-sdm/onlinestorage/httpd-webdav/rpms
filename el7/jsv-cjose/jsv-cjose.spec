Name:           jsv-cjose
Version:        0.6.2.1
Release:        1%{?dist}
Summary:        C library implementing the Javascript Object Signing and Encryption (JOSE)
Group:          System Environment/Libraries
License:        MIT
URL:            https://github.com/zmartzone/cjose
Source0:        https://github.com/zmartzone/cjose/releases/download/v%{version}/cjose-%{version}.tar.gz


BuildRequires:  gcc
BuildRequires:  doxygen
BuildRequires:  openssl-devel
BuildRequires:  jsv-jansson-devel
BuildRequires:  check-devel
BuildRequires:  make

# Exclude libjansson with version information from the automatically
# generated Requires list. libjansson without version will still be required.
%global __requires_exclude ^libjansson\\.so\\..+\\(.+\\)\\(.*\\)$

# cjose and jsv-cjose provide the same files.
Conflicts:      cjose

%description
Implementation of JOSE for C/C++


%package        devel
Summary:        Development files for %{name}
Group:          System Environment/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package devel-doc
Summary:        Development documentation for %{name}
BuildArch:      noarch

%description devel-doc
Development documentation for %{name}.

%prep
%autosetup -p1 -n cjose-%{version}

%build
%configure
%make_build


%install
%make_install
find %{buildroot} -name '*.a' -exec rm -f {} ';'
find %{buildroot} -name '*.la' -exec rm -f {} ';'
rm -r %{buildroot}/usr/share/doc/cjose


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%check
make check || (cat test/test-suite.log; exit 1)

%files
%license LICENSE
%doc CHANGELOG.md README.md
%{_libdir}/*.so.*

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/cjose.pc

%files devel-doc
%doc doc/html

%changelog
* Fri Feb 03 2023 Paul Skopnik <paul.skopnik@kit.edu> - 0.6.2.1-1
- Create initial spec based on Fedora cjose-0.6.1
- Rely on maintenance fork zmartzone/cjose instead of original project
- Rename to jsv-cjose as part of the JSON Symbol Versioning package set
