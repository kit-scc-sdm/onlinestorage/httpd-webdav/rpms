# jsv-liboauth2

This package contains [liboauth2](https://github.com/zmartzone/liboauth2) and aims to closely resemble the official RPM published for each release by the upstream.
It is built against jansson with versioned symbols, although it can use a jansson library without versioned symbols as well.

Both Memcache and Redis support are enabled.
Thus, installation requires the `hiredis` package which is only part of the EPEL repository.
