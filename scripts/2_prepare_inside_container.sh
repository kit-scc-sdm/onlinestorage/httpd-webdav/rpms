

cat - > ./.rpmbuild.sh <<'EOF'
#!/bin/bash
spec_file="$1"
set -ex
dnf builddep -y "$spec_file"
rpmbuild --undefine=_disable_source_fetch -ba "$spec_file"
EOF

echo DONE .rpmbuild.sh


cat - > ./.add-to-buildchain-repo.sh <<'EOF'
#!/bin/bash
set -e
reponame="buildchain"
repodir="/tmp/rpmbuild-buildchain-repo"
mkdir -p "$repodir"
for f in "$@"; do
    cp "$f" "$repodir/"
    echo "Adding $f to buildchain repo"
done
createrepo_c --update "$repodir"
if [ ! -f "/etc/yum.repos.d/buildchain.repo" ]; then
    cat - > /etc/yum.repos.d/buildchain.repo <<EEOF
[buildchain]
name=BuildChain
baseurl=file://$repodir
enabled=1
gpgcheck=0
priority=10
EEOF
fi
# FUTR: This cleans data from all repos (different from yum)
dnf clean all --disablerepo="*" --enablerepo=buildchain
EOF


echo DONE .add-to-buildchain-repo.sh


cat - > ./.add-project-artifacts-to-buildchain-repo.sh <<'EOF'
#!/bin/bash
project_id="$1"
ref="$2"
job="$3"
set -ex
artifacts_zip="$(mktemp)"
curl --get --location --data-urlencode "job=$job" --output "$artifacts_zip" "https://codebase.helmholtz.cloud/api/v4/projects/$project_id/jobs/artifacts/$ref/download"
artifacts_unpacked="$(mktemp -d)"
unzip -d "$artifacts_unpacked" "$artifacts_zip"
rm "$artifacts_zip"
find "$artifacts_unpacked" -path "$artifacts_unpacked"'/artifacts/*/*.rpm' -type f | xargs --no-run-if-empty /.add-to-buildchain-repo.sh
rm -rf "$artifacts_unpacked"
EOF


chmod a+x ./.rpmbuild.sh ./.add-to-buildchain-repo.sh ./.add-project-artifacts-to-buildchain-repo.sh

echo DONE .add-project-artifacts-to-buildchain-repo.sh

sleep 2



echo "keepcache=True" >> /etc/dnf/dnf.conf
dnf install -y rpm-build rpmdevtools dnf-plugins-core createrepo_c
rpmdev-setuptree

dnf config-manager --set-enabled powertools

find artifacts/ -path 'artifacts/*/*.rpm' -type f | xargs --no-run-if-empty ./.add-to-buildchain-repo.sh
RPM_SOURCES_DIR=$(rpm --eval "%{_sourcedir}")



echo DONE dfn1
sleep 2



dnf install -y epel-release


