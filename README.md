# RPMs

This repository contains spec files and build scripts for external RPMs packaging external software.
RPMs are built from the spec files in GitLab CI.

## Contents

### JSON Symbol Versioning (`jsv-`): Conflicting Symbol Names in Several JSON Libraries

The `jsv-` packages are a set of rebuilds where symbol versioning has been enabled for several JSON libraries and their relevant dependents.

Several JSON libraries export their function with names ("symbols") starting with `json_` and there are function with exactly the same name in multiple libraries.
A process may load two of the JSON libraries into memory - usually because of transitive dependencies between libraries.
In such a situation, it is not clear which of the conflicting symbol name is referred to and a call to a function may result in the wrong code to be invoked.
Often, the result are strange errors such as validation errors or eventually segmentation faults.

Symbol versioning ([a](https://johannst.github.io/notes/development/symbolver.html), [b](https://sourceware.org/binutils/docs/ld/VERSION.html)) solves this problem by adding another piece of information - the version - to each symbol at link time.
The version conventionally contains the name of the project, thereby eliminating conflicts between the same function being exported in different projects, as well as the a release version indicator.
Function calls look the same in C code, but symbols written into object files by the linker also contain the version.
This happen both for libraries exporting symbols and depending libraries which use these symbols.
Hence, the shared object (`.so` file) of a library must be available at link time of depending libraries, so that the correct symbol information can be extracted.

Symbol versioning has been introduced to several JSON libraries.
However, not all distributions contain these later releases with version information yet.

 * https://github.com/json-c/json-c/issues/621
   * https://github.com/akheron/jansson/pull/540
   * https://github.com/json-c/json-c/pull/639
   * Also json-glib

The issue of conflicting symbol names is also known in EL, but has been fixed only for EL8.

 * https://bugzilla.redhat.com/show_bug.cgi?id=2001062
   * https://access.redhat.com/errata/RHBA-2022:2061 (only for RHEL8)

## Building Locally

This section shows how to build RPMs locally in a way comparable to the procedure inside GitLab CI.
Local building is recommended during development and testing of spec files.

The approach is to build in a container with the matching OS version.
Within the container, the environment is prepared in a similar way to the rpmbuild CI job templates.
Mounted into the container is

 * a cache directory for yum,
 * a directory for placing artifacts, e.g. built RPMs and
 * the local directory which is most likely the Git repository.

### EL 7

Start the container:

```shell
$ mkdir -p /tmp/yum-cache-el7
$ mkdir -p ../artifacts
$ docker run -it --rm -v /tmp/yum-cache-el7:/var/cache/yum -v `pwd`/../artifacts:/artifacts -v `pwd`:/repo:ro centos:7 bash
```

Within the container:

```shell
cat - > ./.rpmbuild.sh <<'EOF'
#!/bin/bash
spec_file="$1"
set -ex
yum-builddep -y "$spec_file"
rpmbuild --undefine=_disable_source_fetch -ba "$spec_file"
EOF

cat - > ./.add-to-buildchain-repo.sh <<'EOF'
#!/bin/bash
set -e
reponame="buildchain"
repodir="/tmp/rpmbuild-buildchain-repo"
mkdir -p "$repodir"
for f in "$@"; do
    cp "$f" "$repodir/"
    echo "Adding $f to buildchain repo"
done
createrepo_c --update "$repodir"
if [ ! -f "/etc/yum.repos.d/buildchain.repo" ]; then
    cat - > /etc/yum.repos.d/buildchain.repo <<EEOF
[buildchain]
name=BuildChain
baseurl=file://$repodir
enabled=1
gpgcheck=0
priority=10
EEOF
fi
yum clean all --disablerepo="*" --enablerepo=buildchain
EOF

cat - > ./.add-project-artifacts-to-buildchain-repo.sh <<'EOF'
#!/bin/bash
project_id="$1"
ref="$2"
job="$3"
set -ex
artifacts_zip="$(mktemp)"
curl --get --location --data-urlencode "job=$job" --output "$artifacts_zip" "https://codebase.helmholtz.cloud/api/v4/projects/$project_id/jobs/artifacts/$ref/download"
artifacts_unpacked="$(mktemp -d)"
unzip -d "$artifacts_unpacked" "$artifacts_zip"
rm "$artifacts_zip"
find "$artifacts_unpacked" -path "$artifacts_unpacked"'/artifacts/*/*.rpm' -type f | xargs --no-run-if-empty /.add-to-buildchain-repo.sh
rm -rf "$artifacts_unpacked"
EOF

chmod a+x ./.rpmbuild.sh ./.add-to-buildchain-repo.sh ./.add-project-artifacts-to-buildchain-repo.sh

sed -i 's|keepcache=0|keepcache=1|' /etc/yum.conf
yum install -y rpm-build rpmdevtools yum-utils createrepo_c yum-plugin-priorities
rpmdev-setuptree
find artifacts/ -path 'artifacts/*/*.rpm' -type f | xargs --no-run-if-empty ./.add-to-buildchain-repo.sh
RPM_SOURCES_DIR=$(rpm --eval "%{_sourcedir}")


# Specify a reasonable job name
JOB_NAME_SLUG=myjob
mkdir "artifacts/$JOB_NAME_SLUG"


# This next part can be repeated during development and testing:


# Enable EPEL repository
yum install -y epel-release
# Prepare $RPM_SOURCES_DIR
cp /repo/somefile $RPM_SOURCES_DIR/
# Build spec
./.rpmbuild.sh /repo/mypackage.spec


cp /root/rpmbuild/RPMS/*/* /root/rpmbuild/SRPMS/* "artifacts/$JOB_NAME_SLUG/"
```

### EL 8

Start the container:

```shell
$ mkdir -p /tmp/yum-cache-el8
$ mkdir -p ../artifacts
$ docker run -it --rm -v /tmp/yum-cache-el8:/var/cache/dnf -v `pwd`/../artifacts:/artifacts -v `pwd`:/repo:ro rockylinux:8 bash
```

Within the container:


```shell
cat - > ./.rpmbuild.sh <<'EOF'
#!/bin/bash
spec_file="$1"
set -ex
dnf builddep -y "$spec_file"
rpmbuild --undefine=_disable_source_fetch -ba "$spec_file"
EOF

cat - > ./.add-to-buildchain-repo.sh <<'EOF'
#!/bin/bash
set -e
reponame="buildchain"
repodir="/tmp/rpmbuild-buildchain-repo"
mkdir -p "$repodir"
for f in "$@"; do
    cp "$f" "$repodir/"
    echo "Adding $f to buildchain repo"
done
createrepo_c --update "$repodir"
if [ ! -f "/etc/yum.repos.d/buildchain.repo" ]; then
    cat - > /etc/yum.repos.d/buildchain.repo <<EEOF
[buildchain]
name=BuildChain
baseurl=file://$repodir
enabled=1
gpgcheck=0
priority=10
EEOF
fi
# FUTR: This cleans data from all repos (different from yum)
dnf clean all --disablerepo="*" --enablerepo=buildchain
EOF

cat - > ./.add-project-artifacts-to-buildchain-repo.sh <<'EOF'
#!/bin/bash
project_id="$1"
ref="$2"
job="$3"
set -ex
artifacts_zip="$(mktemp)"
curl --get --location --data-urlencode "job=$job" --output "$artifacts_zip" "https://codebase.helmholtz.cloud/api/v4/projects/$project_id/jobs/artifacts/$ref/download"
artifacts_unpacked="$(mktemp -d)"
unzip -d "$artifacts_unpacked" "$artifacts_zip"
rm "$artifacts_zip"
find "$artifacts_unpacked" -path "$artifacts_unpacked"'/artifacts/*/*.rpm' -type f | xargs --no-run-if-empty /.add-to-buildchain-repo.sh
rm -rf "$artifacts_unpacked"
EOF

chmod a+x ./.rpmbuild.sh ./.add-to-buildchain-repo.sh ./.add-project-artifacts-to-buildchain-repo.sh

echo "keepcache=True" >> /etc/dnf/dnf.conf
dnf install -y rpm-build rpmdevtools dnf-plugins-core createrepo_c
rpmdev-setuptree
dnf config-manager --set-enabled powertools
find artifacts/ -path 'artifacts/*/*.rpm' -type f | xargs --no-run-if-empty ./.add-to-buildchain-repo.sh
RPM_SOURCES_DIR=$(rpm --eval "%{_sourcedir}")


# Specify a reasonable job name
JOB_NAME_SLUG=myjob
mkdir "artifacts/$JOB_NAME_SLUG"



# Enable EPEL repository
dnf install -y epel-release
# Prepare $RPM_SOURCES_DIR
cp /repo/somefile $RPM_SOURCES_DIR/
# Build spec
./.rpmbuild.sh /repo/mypackage.spec


cp /root/rpmbuild/RPMS/*/* /root/rpmbuild/SRPMS/* "artifacts/$JOB_NAME_SLUG/"
```

## Relevant Bits

 * For a single yum invocation, only look at some repos
   * `yum --disablerepo="*" --enablerepo="<desired-repo-id>" install <package>`
 * `includepkgs` and `exclude` in a repository's configuration to only regard some of its packages
   * https://unix.stackexchange.com/questions/12715/how-to-only-allow-exact-package-updates-from-a-repository
 * yum-plugin-priorities to prefer one repository's packages over other repositories
   * https://wiki.centos.org/PackageManagement/Yum/Priorities
   * Default priority is `99`, the repository with the lowest priority value is preferred
 * Download sources for a `.spec` file
   * https://stackoverflow.com/questions/33177450/how-do-i-get-rpmbuild-to-download-all-of-the-sources-for-a-particular-spec
 * `keepcache=1` in `/etc/yum.conf` to keep installed/downloaded RPM package files
   * https://man7.org/linux/man-pages/man5/yum.conf.5@@yum.html
 * Upstream `.spec` sources
   * CentOS https://git.centos.org/projects/rpms/%2A
   * Fedora, including EPEL: https://src.fedoraproject.org/projects/rpms/%2A
 * RPM File meta data
   * `rpm -qp /repo/cjose-0.6.2.1-1.el7.x86_64.rpm --qf "$(rpm --querytags | sed -nr 's/(.*)/\1:%{\1}/p' | tr '[:space:]' '\n')"`

## Licensing

Except if explicitly noted otherwise, all content in this repository is licensed under the MIT License (see LICENSE file).
Much of the spec files is based on or derived from spec files in the CentOS and Fedora projects where the spec files and related code is also under the MIT License.
Change history including some authorship information is available within the `%changelog` section, additionally the original source of the files is described in README files.
Detailed authorship information of changes performed in this repository is part of the Git commit data.
